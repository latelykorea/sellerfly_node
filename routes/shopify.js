var express = require('express');
const Common = require("../processor/shopify/common");
var router = express.Router();
const {ShopifyApi} = require('../processor')
const {redis} = require('../processor/libs/redis');
const _ = require('lodash')

const MISSING_FIELD = (filed) => {
  return {
    filed: filed,
    msg: `missing filed`
  }
}

const INVALID_FIELD = (field) => {
  return {
    field: field,
    msg: 'invalid field'
  }
}

router.get('/products', async (req, res, next) => {
  try {
    if (!req.query.shop) {
      return res.status(400).send(MISSING_FIELD('shop'));
    }
    let resp = await ShopifyApi.Products.getProductList({shop: `${req.query.shop}.myshopify.com`});
    res.status(200).send(resp);
  } catch (e) {
    res.status(500).send(e);
  }
})

router.post('/products', async (req, res, next) => {
  try {
    // console.log(req.body);
    console.log(req.body);
    let shop = req.body.shopUrl;
    let products = req.body.products;
    // validator
    // console.log(products);
    console.log(Object.keys(req.body));
    if (!shop) {
      return res.status(400).send(MISSING_FIELD('shopUrl'));
    }
    if (!await Common.isExistShop(shop)) {
      return res.status(404).send('Not Found Shop');
    }
    if (!products) {
      return res.status(400).send(MISSING_FIELD('products'));
    }
    if (!Array.isArray(products) || products.length === 0) {
      return res.status(200).send(INVALID_FIELD('products'))
    }

    let resp = await ShopifyApi.Products.createProduct({shop, goods: req.body.products})
    return res.status(200).send(resp);

  } catch (e) {
    return res.status(500).send(e);
  }
})

router.get('/products/jobs/:jobSeq', async (req, res, next) => {

  let jobIdList = await redis.smembers(`map_product_${req.params.jobSeq}`);
  if (jobIdList.length === 0) {
    return res.status(404).send('Not Found Job Sequence.');
  }

  let _data = await redis.hgetall(`result_product_${req.params.jobSeq}`);

  // console.log(_data);
  let _body = {
    seq: req.params.jobSeq,
    results: _.values(_data).map(JSON.parse)
  }

  res.status(200).send(_body);


})

router.use('/orders', require('./shopify/orders'));

// router.get('/orders', async (req, res, next) => {
//
//   try {
//     res.status(200).send(await ShopifyApi.Orders.getOrders());
//   } catch (e) {
//     res.status(500).send(e);
//   }
//
// })

router.use('/merchants/:name/orders', (req, res, next) => {
  req.shopName = getShopUrl(req.params.name);

  next()
}, require('./shopify/orders'))

const getShopUrl = (name) => {return `${name}.myshopify.com`}


module.exports = router;