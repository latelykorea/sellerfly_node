require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

var cors = require('cors');
var whitelist = ['*']

var corsOptions = {
  origin: function(origin, callback){
    var isWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(null, isWhitelisted);
    // callback expects two parameters: error and options
  },
  credentials:true
}

const indexRouter = require('./routes/index');

const app = express();

app.use( cors(corsOptions) );

// shopify config
const session = require('express-session');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

const swaggerDoc = require('./libs/swagger-docs');

app.use(swaggerDoc);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
//   secret: 'your session secret',
//   resave: false,
//   saveUninitialized: true
// }));

const redis = require('redis');
const {REDIS_HOST, REDIS_PORT} = process.env;
let RedisStore = require('connect-redis')(session)

const client = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT
});

app.use('/', indexRouter);
app.use('/shopify', require('./routes/shopify'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
