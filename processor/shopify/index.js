'use strict';

// export {default as Products} from './products'
// export {default as Orders} from './orders';

module.exports = {
  Products: require('./products'),
  Orders: require('./orders')
}