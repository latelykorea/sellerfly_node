require('dotenv').config();
const {
  Worker,
  isMainThread,
  parentPort,
  workerData
} = require('worker_threads');

const Common = require('../common');
const logger = require('../../libs/logger');
const _ = require('lodash');

const axios = require('axios');
const http = axios.create({
  baseURL: process.env["API_HOST"] || 'https://ob.sellerhub.co.kr'
})

const LIMIT = 10;
const VENDOR = process.env.VENDOR || 'sellerfly'

parentPort.on('message', async (shop) => {

  const {name, sinceId} = shop;
  logger.info(`${workerData.name}   Start collecting order for shop: ${name} from event id ${sinceId}`)
  const shopify = await Common.getShopifyInstance(name);

  try {

    let eventParam = {
      filter: 'Order',
      since_id: sinceId,
      limit: LIMIT
      // verb: 'cancelled'
    };

    shopify.event.list(eventParam)
      .then((events) => {
        return new Promise((resolve, reject) => {
          const next = {};
          // parse event type;
          if (events.length === 0) {
            parentPort.close();
            reject(`${workerData.name} No has new events.`)
          }

          _.map(_.sortBy(events, ['id', 'subject_id']), e => {
            next[e.subject_id] = {};
            next[e.subject_id].event = e;
          })
          next.lastEventId = _.last(events)['id']
          next.name = name;
          resolve(next);
        })
      })
      .then(async list => {
        const ids = Object.keys(list).map(id => parseInt(id));
        // console.log(ids.filter(id => !isNaN(id)).join(','));
        return new Promise((resolve, reject) => {
          let orderParams = {
            ids: ids.filter(id => !isNaN(id)).join(','),
            // ids: '3038704238742,3038708727958,3040583483542'
            // ids,
            // limit: LIMIT
          }
          shopify.order.list(orderParams)
            .then(orders => {
              // convert and filtering our product order
              // filter #1
              _.map(orders, (order) => {
                order.line_items = order.line_items.filter((item) => {
                  return item.vendor === VENDOR
                });
                return order;
              })
                .filter(order => order.line_items.length > 0)
                // set
                .forEach(order => {
                  list[order.id]['order'] = order
                })

              let includeOurItem = Object.keys(list)
                .filter(key => {
                  return list[key].hasOwnProperty('order')
                });

              return resolve(Object.fromEntries(Object.entries(list).filter(([k, v]) => {
                return includeOurItem.includes(k) || k === 'lastEventId' || k === 'name';
              })));
            })
        })
      })
      // send api
      .then(sendOrder)
      .then(postMsg)
      .catch(reason => {
        console.log(`[${workerData.name}]` + JSON.stringify(reason, null, 2));
        // logger.warn(JSON.stringify(reason, null, 2));
        parentPort.close();
      })
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }

})

const postMsg = (list) => {
  parentPort.postMessage(list)
  // parentPort.close();
  logger.info(`[${workerData.name}]   End collecting order for shop: ${workerData.name} eventId: ${list.lastEventId}`)
}

const deleteAllOrders = (shopify, results) => {
  _.map(results, 'id').forEach(async id => console.log(shopify.order.delete(id)));
}

const sendOrder = async (list) => {
  return new Promise((resolve, reject) => {
    let orders = [];
    Object.keys(list).filter(k => !(k === 'lastEventId' || k === 'name')).map(k => {
      orders.push(list[k].order);
    });
    let model = new Model(list['name'], list['lastEventId'], orders);
    model.shortPrint();
    // console.log(JSON.stringify(model, null, 2))
    http.post('/api/carts', model)
      .then(resp => {
        // console.log(resp.data);
        // reject();
        resolve(list)
      })
      .catch(reason => {
        console.log(`[${workerData.name}]` + reason.response.data.message);
        // resolve(list)
        // reject(reason.response);
      })

  })
}

function Model(shopUrl, lastEventId, orders) {
  this.shopUrl = shopUrl;
  this.lastEventId = lastEventId
  this.orders = orders;
}

Model.prototype.shortPrint = function () {
  const obj = {
    shopUrl: this.shopUrl,
    lastEventId: this.lastEventId,
    orders: _.map(this.orders, order => {
      return {
        id: order.id,
        orderNo: order.name,
        lienItems: _.map(order.line_items, ({product_id}) => {
          return (product_id)
        })}
    })
  };

  console.log(JSON.stringify(obj, null, 2))
}

