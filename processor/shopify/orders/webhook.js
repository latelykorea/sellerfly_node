require('dotenv').config();
const {redis} = require('../../libs/redis');
const INTERVAL = 1000;
const _ = require('lodash');
const {Worker, isMainThread} = require('worker_threads');
const logger = require('../../libs/logger');
const moment = require('moment');
const Common = require('../common');

async function WebHook(shopUrl = 'jongho-test.myshopify.com') {
  return new Promise((resolve, reject) => {
    resolve(Common.getShopifyInstance(shopUrl));
  })
}

async function listen(shopify) {
  return new Promise(resolve => {
    let address = 'https://ob.sellerhub.co.kr/node/shopify/webhook'
    let params = {
      // address: string;
      // fields?: string[];
      // format?: WebhookFormat;
      // metafield_namespaces?: string[];
      // topic: WebhookTopic;
      address,
      topic: 'orders/create',
      // topic: 'orders/cancelled',
      format: 'json'
    }
    // resolve(shopify.webhook.create(params))
    // console.log(JSON.stringify(shopify, null, 2));
    // resolve(shopify.webhook.delete(996517707926));
    resolve(shopify.webhook.list({}))
    // resolve(shopify.product.list({limit: 1}));
  })
}

// 'app/uninstalled'
// 'carts/create'
// 'carts/update'
// 'checkouts/create'
// 'checkouts/delete'
// 'checkouts/update'
// 'collection_listings/add'
// 'collection_listings/remove'
// 'collection_listings/update'
// 'collections/create'
// 'collections/delete'
// 'collections/update'
// 'customer_groups/create'
// 'customer_groups/delete'
// 'customer_groups/update'
// 'customers/create'
// 'customers/delete'
// 'customers/disable'
// 'customers/enable'
// 'customers/update'
// 'draft_orders/create'
// 'draft_orders/delete'
// 'draft_orders/update'
// 'fulfillment_events/create'
// 'fulfillment_events/delete'
// 'fulfillments/create'
// 'fulfillments/update'
// 'inventory_items/create'
// 'inventory_items/update'
// 'inventory_items/delete'
// 'inventory_levels/connect'
// 'inventory_levels/update'
// 'inventory_levels/disconnect'
// 'locations/create'
// 'locations/update'
// 'locations/delete'
// 'order_transactions/create'
// 'orders/cancelled'
// 'orders/create'
// 'orders/delete'
// 'orders/fulfilled'
// 'orders/paid'
// 'orders/partially_fulfilled'
// 'orders/updated'
// 'product_listings/add'
// 'product_listings/remove'
// 'product_listings/update'
// 'products/create'
// 'products/delete'
// 'products/update'
// 'refunds/create'
// 'shop/update'
// 'themes/create'
// 'themes/delete'
// 'themes/publish'
// 'themes/update';



async function register(shopify) {
  return new Promise(resolve => {
    let params = {
      filter: 'Order',
      // filter: 'Authorization, Capture, Email, Fulfillment, Order, Refund, Sale, Void',
      limit: '250',
      // subject_type: 'Order',
      // since_id: '66904664047766',
      verb: 'cancelled'
    }

    resolve(shopify.event.list(params))
  });
}

async function test(shopify) {
  const query = `
  query customers {
  orders(first: 1) {
    edges {
      node {
        events(first: 10) {
          edges {
            node {
              ... on BasicEvent {
                id
                message
                criticalAlert
                createdAt
                attributeToUser
                attributeToApp
                appTitle
              }
              ... on CommentEvent {
                id
                embed {
                  ... on Order {
                    id
                    email
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}  
  `
  return new Promise(resolve => {
    resolve(shopify.graphql(query))
  })
}

function print(obj) {
  console.log(JSON.stringify(obj, null, 2));
}

// WebHook().then(register).then(print)
WebHook().then(listen).then(print)



// WebHook().then(test).then(print);


