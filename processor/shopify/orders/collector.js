require('dotenv').config();
const {redis} = require('../../libs/redis');
const INTERVAL = 6 * 10 * 1000;
const _ = require('lodash');
const { Worker, isMainThread } = require('worker_threads');
const logger = require('../../libs/logger');
const moment = require('moment');

const Collector = {
  shopList: [],
  orders: async () => {
    Collector.shopList = await redis.keys('*.myshopify.com')
    // Collector.shopList = await redis.keys('jongho-test.myshopify.com')

    for (const shop of Collector.shopList) {
      const worker = new Worker('./processor/shopify/orders/worker_rest.js', {workerData: {name: shop}});
      worker.postMessage({name: shop, sinceId: await getLastEventId(shop)});

      worker.on('message', async (incoming) => {
        const {lastEventId, name} = incoming;
        delete incoming.lastEventId;

        console.log(name, lastEventId);
        await redis.hmset('last_event_id', [name, lastEventId])
      })

      try {
        worker.on("error", code => new Error(`Worker error with exit code ${code}`));
        worker.on("exit", code =>
          console.log(`Worker stopped with exit code ${code}`)
        );
      } catch (e) {
        console.log(e);
      }
    }
  }
}

const getLastEventId = async (shop) => {
  return await redis.hget('last_event_id', shop);
}

Collector.orders();
// setInterval(() => Collector.__init(), INTERVAL);

// module.exports = {
//   Shop
// }





