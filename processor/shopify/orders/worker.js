const {
  Worker,
  isMainThread,
  parentPort
} = require('worker_threads');

const Common = require('../common');
const logger = require('../../libs/logger');
const _ = require('lodash');

const fetch = require("node-fetch");

// const {Shop} = require('./collector')

// console.log('isMainThread:', isMainThread);

parentPort.on('message', async (shop) => {

  const {name, updatedDt} = shop;
  logger.info(`Start collecting order for shop: ${name} from ${updatedDt}`)
  const shopify = await Common.getShopifyInstance(name);

  const query = `query Orders($cursor: String) {
  orders(first: 2, after: $cursor, query:"updated_at>:${updatedDt}") {
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
   edges {
      cursor
      node {
        id
        name
        updatedAt
        lineItems(first: 100) {
          pageInfo {
            hasNextPage
            hasPreviousPage
          }
          edges {
            cursor
            node {
              id
              title
              vendor
              quantity
              variantTitle
              variant {
                id
                title
              }
            }
          }
        }
        closed
        closedAt
        confirmed
        createdAt
        currencyCode
        fulfillable
        shippingAddress {
          address1
          address2
          city
          country
          company
          countryCode
          countryCodeV2
          firstName
          formattedArea
          id
          lastName
          latitude
          name
          longitude
          phone
          province
          provinceCode
          zip
        }
        test
      }
    }
  }
}
`
  const query2 = `query items($id: ID!, $after: String) 
{
  order(id: $id) {
    name
    lineItems(first: 10, after: $after) {
      pageInfo {
        hasNextPage
      }
      edges {
        node {
          id
          title
          vendor
        }
      }
    }
  }
}`

  let param = {
    updated_at_min: updatedDt,
    // attribution_app_id: 'current',
    status: 'any'
  }
  let resp = await shopify.order.list(param)
  // let t = await shopify.order.list({updated_at_min: updatedDt})
  console.log(JSON.stringify(resp, null, 2))
  // console.log(_.map(resp, ))
  let orderIds = _.map(resp, 'id');

  // orderIds.forEach(async id => {
  //   console.log(await shopify.order.delete(id));
  // })

  console.log(orderIds);
  return parentPort.postMessage({name, lastDt, orders: orderIds})

  // let cnt = 0;
  let orderCursor = undefined;
  let hasNextOrder = false;
  let collectedOrders = [];
  let lastDt = updatedDt;
  try {
    do {
      // console.log(`${++cnt} times `)
      const params = {
        cursor: orderCursor,
      }
      let response = await shopify.graphql(query, params);
      hasNextOrder = response.orders.pageInfo.hasNextPage;

      collectedOrders.push(_.map(response.orders.edges, edge => {
        orderCursor = edge.cursor
        let orderId = edge.node.id
        lastDt = edge.node.updatedAt;

        let items = _.map(edge.node.lineItems.edges, 'node').filter(item => item.vendor === 'sellerfly');
        // console.log(items);
        if (items.length > 0) {
          const Order = edge.node;
          delete Order.lineItems
          Order.items = items;
          return Order;
        }
        // let lienItems = [];

        // console.log(edge.node.lineItems.pageInfo.hasNextPage);
        // while (edge.node.lineItems.pageInfo.hasNextPage) {
        //   shopify.graphql(query2, {id: orderId, cursor: ''})
        // }
        return undefined;
        // return edge.node;
      }).filter(order => order));


      collectedOrders = collectedOrders.flat();

    } while (hasNextOrder)
  } catch (e) {
    console.log(e);
  }

  // try {
  //   console.log(JSON.stringify(collectedOrders, null, 2));
  //   console.log(_.map(collectedOrders, 'items').map(item => item.length));
  // } catch (e) {
  //   console.log(e);
  // }


  // const params = {
  //   // after: orderCursor,
  //   // query: `updated_at>:2021-01-10`
  // }
  // let collectedOrders = await shopify.graphql(query, params);
  // console.log(JSON.stringify(collectedOrders, null, 2))

  // let orders = _.map(collectedOrders.orders.edges, edge => {
  //   orderCursor = edge.cursor;
  //   console.log(orderCursor);
  //   let items = _.map(edge.node.lineItems.edges, 'node').filter(item => item.vendor === 'sellerfly');
  //   if (items.length > 0) {
  //     const Order = edge.node;
  //     delete Order.lineItems
  //     Order.items = items;
  //     return Order;
  //   }
  // }).filter(order => order != null);

  // console.log(orderCursor);
  logger.info(`End collecting order for shop: ${name}`)
  // parentPort.postMessage({name, lastDt, orders: collectedOrders})
  // console.log(JSON.stringify(orders, null, 2))
  // return orders;
  // parentPort.postMessage({name: shop, orders})

});

// const filter = (orders) => {
//   return _.map(collectedOrders.orders.edges, edge => {
//     orderCursor = edge.cursor;
//     let items = _.map(edge.node.lineItems.edges, 'node').filter(item => item.vendor === 'sellerfly');
//     if (items.length > 0) {
//       const Order = edge.node;
//       delete Order.lineItems
//       Order.items = items;
//       return Order;
//     }
//   }).filter(order => order != null);
//
// }