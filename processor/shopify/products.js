require('dotenv').config();
const _ = require('lodash');
const logger = require('../libs/logger');
const Common = require('./common');
const ProductModel = require("./models/ProductModel");
const {redis} = require('../libs/redis');

const Products = {
  getProductList: async ({shop: shopUrl}) => {
    let shopify = await Common.getShopifyInstance(shopUrl);
    try {
      return await shopify.product.list({vendor: 'sellerfly'});

    } catch (e) {
      logger.warn(e);
      return e;
    }
  },

  createProduct: async ({shop: shopUrl, goods: goods}) => {
    try {
      const seq = await redis.incr('product_seq');
      const queueConf = {
        redis: {
          host: process.env.REDIS_HOST || '127.0.0.1',
          port: process.env.REDIS_PORT || 6379,
          db: 0
        }
      }

      const Queue = require('bee-queue');
      const q = new Queue('product', queueConf);

      _.map(goods, g => {
        g.seq = seq;
        g.shopUrl = shopUrl;

        // BeeQueue
        let job = q.createJob(g);
        job.timeout(30 * 1000).retries(0).save().then(
          r => {
            // console.log(`jobId: ${r.id}`);
            redis.sadd(`map_product_${seq}`, r.id);
          });
      })

      return {seq};
      // let shopify = await Common.getInstance(shopUrl);
      // let product = new ProductModel(goods);
      // // return product;
      // return await shopify.product.create(product);
    } catch (e) {
      return e;
    }
  },

  orders: async ({shop: shopUrl}) => {
    try {
      let shopify = await Common.getShopifyInstance(shopUrl);

      const query = `{
      
      orders(first: 10, query:"updated_at:>2019-12-01") {
      pageInfo {
              hasNextPage,
              hasPreviousPage
            }
      edges {
        cursor
        node {
          ## Order
          id
          name
          lineItems(first: 10) {
            ## Ordered Items
            pageInfo {
              hasNextPage,
              hasPreviousPage
            }
            edges {
              node {
                id
                title
                vendor
              
              }
            }
          }
        }
      }
  }
  }
  `
      let collectedOrders = await shopify.graphql(query);
      // console.log(JSON.stringify(r, null, 2));
      console.log(collectedOrders.orders.edges.length);
      let orderCursor = undefined;
      let orders = _.map(collectedOrders.orders.edges, edge => {
        orderCursor = edge.cursor;
        let items = _.map(edge.node.lineItems.edges, 'node').filter(item => item.vendor === 'sellerfly');
        if (items.length > 0) {
          const Order = edge.node;
          delete Order.lineItems
          Order.items = items;
          return Order;
        }
      }).filter(order => order != null);
      console.log(orderCursor);
      return orders;
    } catch (e) {
      return e;
    }
  }
}

// graphQL Test
const graphQL = async () => {

  let shopify = await Common.getShopifyInstance();
  // logger.info(shopify);
  const query = `{
  products(first: 10, query: "product_type:Snowboard") {
    edges {
      node {
        title
      }
    }
  }

  }`;
  try {

    let resp = await shopify.graphql(query);
    logger.info(resp.products.edges);
  } catch (e) {
    console.log('error');
    // console.log(e);
  }
  // try {
  //   let resp = await shopify.product.list({
  //     limit: 2,
  //     metafields_global_title_tag:'Snowboard'/Users/myeongjongho/workspaces/sellerhub/nodejs/sellerfly/product.json
  //   });
  //   console.log(_.map(resp, 'title'));
  //
  // } catch (e) {
  //   console.log('err')
  // }
  ///Users/myeongjongho/workspaces/sellerhub/nodejs/sellerfly/product.json
  // // line_items
  // try {
  //   let result = await shopify.order.list();
  //   console.log(JSON.stringify(result, null, 2));
  //   console.log()
  // } catch (e) {
  //   console.log('error order');
  // }

}

module.exports = Products;