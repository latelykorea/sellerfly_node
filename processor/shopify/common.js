const Shopify = require("shopify-api-node");
const logger = require("../libs/logger");
const {TokenManager} = require("../libs/redis");
const {redis} = require('../libs/redis');

const Common = {
  getShopifyInstance: async (shop) => {
    return !await Common.isExistShop(shop) ? new Error("Not defined shop url.") : new Shopify({
      shopName: shop,
      accessToken: await TokenManager.get(shop)
    });
  },

  getShopify: async (shop) => {
    return new Promise((resolve, reject) => {
      Common.isExistShop(shop)
        .then((conf) => {
          resolve(new Shopify(conf))
        }).catch(reject)
    })
  },

  getQueueConf: () => {
    return {
      prefix: 'bq',
      stallInterval: 5000,
      nearTermWindow: 1200000,
      delayedDebounce: 1000,
      redis: {
        host: '127.0.0.1',
        port: 6379,
        db: 0,
        options: {},
      },
      isWorker: true,
      getEvents: true,
      sendEvents: true,
      storeJobs: true,
      ensureScripts: true,
      activateDelayedJobs: false,
      removeOnSuccess: false,
      removeOnFailure: false,
      redisScanCount: 100,
    }
  },

  isExistShop: async (shop) => {
    return new Promise((resolve, reject) => {
      redis.exists(shop).then(result => {
        if (result === 0) {
          return reject('Not defined shop url.')
        }
        TokenManager.get(shop).then(accessToken => {
          resolve({shopName: shop, accessToken})
        })
      });
    })
    // if (await redis.exists(shop) === 1) {
    //   return true
    // }

  }
};

module.exports = Common

