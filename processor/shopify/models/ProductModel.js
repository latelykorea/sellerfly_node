require('dotenv').config({});
const data = require('../sample-json/send_data_20210105.json');
const _ = require('lodash');

const VENDOR = process.env.VENDOR || 'sellerfly';
const STATUS = process.env.PRODUCT_STATUS || 'active';


function ProductModel(props) {
  // console.log(props);
  const variants = _.map(props.ob_send_product_options, option => {
    const result = {
      inventory_management: 'shopify',
      inventory_quantity: option.stock || 28,
      option1: option.ob_option1,
      price: option.price,
      currency_code: 'USD',
      presentment_prices: [
        {
          price: {
            amount: option.price,
            currency_code: option.currency.toUpperCase()
          },
        }
      ]
    }

    if (option.ob_option2) {
      result.option2 = option.ob_option2;
    }

    return result;
  })

  const optValues1 = _.map(variants, 'option1');
  const optValues2 = _.map(variants, 'option2');

  const options = [];
  options.push({
    name: props.ob_send_product_options[0].ob_option_name1,
    values: optValues1
  });

  if (props.ob_send_product_options[0].ob_option2) {
    options.push({
      name: props.ob_send_product_options[0].ob_option_name2,
      values: optValues2
    })
  }

  this.body_html = props.longdesc_eng || undefined;
  this.images = ((images) => {
    return images.map(src => {
      return {
        src
      }
    })
  })(props.imgs);;

  this.variants = variants;
  this.options = options;

  this.product_type =  props.productType || undefined;
  // this.published_scope = published_scope;
  // this.tags = props.tags || [];
  // this.template_suffix = template_suffix;
  this.title = props.ob_product_name || undefined;
  // this.updated_at = updated_at;
  this.vendor = props.vender || VENDOR;
  this.status = props.status || STATUS;

}

function ProductVariants (props) {

  this.barcode = "";
  this.compare_at_price = undefined;
  this.created_at = "";
  this.fulfillment_service = "";
  this.grams = 0;
  this.id = 0;
  this.image_id = undefined;
  this.inventory_item_id = 0;
  this.inventory_management = "";
  this.inventory_policy = undefined;
  this.inventory_quantity = 0;
  this.old_inventory_quantity = 0;
  this.option1 = undefined;
  this.option2 = undefined;
  this.option3 = undefined;
  this.position = 0;
  this.presentment_prices = [];
  this.price = "";
  this.product_id = 0;
  this.requires_shipping = false;
  this.sku = "";
  this.tax_code = undefined;
  this.taxable = false;
  this.title = "";
  this.updated_at = "";
  this.weight = 0;
  this.weight_unit = undefined;

}

function ProductOption({name, values=[]}) {
  this.name = name
  this.values = values

  // read only
  // this.id = undefined;
  // this.position = undefined;
  // this.product_id = undefined;
}

function ProductImage(props) {
  this.src = undefined;
  this.height = undefined;
  this.width = undefined;
  this.alt = undefined;
  this.position = undefined;

  // read-only
  // this.created_at = undefined;
  // this.id = undefined;
  // this.product_id = undefined;
  // this.updated_at = undefined;
  // this.variant_ids = undefined;
}

function test() {
  let p = new ProductModel(data.products[0]);
  console.log(JSON.stringify(p, null, 2));
  // console.log(p.options);
}

// test();

module.exports = ProductModel;



