const axios = require('axios');
const {redis} = require('../../libs/redis');
const _ = require('lodash');
const http = axios.create({
  baseURL: 'https://ob.sellerhub.co.kr'
})

async function t(seq) {
  let _data = await redis.hgetall(`result_product_${seq}`);
  let _body = {
    seq: seq,
    shopUrl: 'shop',
    results: _.values(_data).map(JSON.parse)
  }
  console.log(_body);

  await http.post('/api/sendProduct/modify/refProductId', _body).then(console.log).catch(console.log);
}

t(18);
