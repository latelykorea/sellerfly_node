// require('dotenv').config();
const queueConf = {
  redis: {
    host: '127.0.0.1',
    port: 6379,
    db: 0,
    options: {},
  }
}

const Queue = require('bee-queue');
const q = new Queue('product', queueConf);
const _ = require('lodash');
const Common = require("../common");
const ProductModel = require("../models/ProductModel");

const {redis} = require('../../libs/redis');
const axios = require('axios');
const http = axios.create({
  baseURL: 'https://ob.sellerhub.co.kr'
})
http.defaults.headers.post['Content-Type'] = 'application/json';

async function filter(arr, callback) {
  const fail = Symbol()
  return (await Promise.all(arr.map(async item => (await callback(item)) ? item : fail))).filter(i=>i!==fail)
}

function makeBody() {

}

const onSucceeded = async (job, result) => {
  console.log(`Job ${job.id} succeeded with result: ${JSON.stringify(result, null, 2)}`);

  const data = {
    id: job.data.id,
    shopify: result.productId
  }
  await redis.hmset([`result_product_${job.data.seq}`, job.data.id, JSON.stringify(data)]);

  let remain = await redis.sdiff(`map_product_${job.data.seq}`, 'bq:product:succeeded', 'bq:product:failed');

  if (remain.length === 0) {

    let _data = await redis.hgetall(`result_product_${job.data.seq}`);
    let _body = {
      seq: job.data.seq,
      shopUrl: job.data.shopUrl,
      results: _.values(_data).map(JSON.parse)
    }
    // console.log(_body);

    try {
      let resp = await http.post('/api/sendProducts/refProductId', JSON.stringify(_body));
      console.log(`Job Seq ${job.data.seq} succeed send~!`);
    } catch (e) {
      console.log(e.data.message)
    }

  }

}

const onFailed = async (jobId, err) => {
  let job = await q.getJob(jobId);

  const data = {
    id: job.data.id,
    shopify: -1,
    message: err.message
  }
  await redis.hmset([`result_product_${job.data.seq}`, job.data.id, JSON.stringify(data)]);

  let remain = await redis.sdiff(`map_product_${job.data.seq}`, 'bq:product:failed', 'bq:product:succeeded');

  if (remain.length === 0) {
    console.log(`Job Seq ${job.data.seq} failed send~!`);

    let _data = await redis.hgetall(`result_product_${job.data.seq}`);
    let _body = {
      seq: job.data.seq,
      shopUrl: job.data.shopUrl,
      results: _.values(_data).map(JSON.parse)
    }

    // console.log(_body);

    try {
      let resp = await http.post('/api/sendProducts/refProductId', JSON.stringify(_body));
      console.log(`Job ${jobId} failed with ${err.message}`);
    } catch (e) {
      console.log(e.data.message)
    }

  }
};

const onProcess = async (job) => {
  console.log(`Processing job ${job.id}`);

  let shopify = await Common.getShopifyInstance(job.data.shopUrl);
  shopify.on('callLimits', (limits) => console.log(`limits : ${JSON.stringify(limits)}`));
  let product = new ProductModel(job.data);
  // console.log(JSON.stringify(product));

  try {
    let resp = await shopify.product.create(product);
    return Promise.resolve({
      productId: resp.id
    })
    // console.log(resp);
  } catch (e) {
    // console.log(e);
    return Promise.reject(new Error(e.message))
  }
}

q.on('ready', () => {
  console.log('register on ready...');

  q.on('job failed', onFailed);

  q.on('succeeded', onSucceeded);

  q.process(10, onProcess);
})

// q.on('job progress', (jobId, progress) => {
//   console.log('q.on');
//   // console.log(jobId)
//   console.log(`Job ${jobId} reported progress: ${progress}%`);
// });

// q.checkStalledJobs(5000, (err, numStalled) => {
//   // prints the number of stalled jobs detected every 5000 ms
//   console.log('Checked stalled jobs', numStalled);
// });