require('dotenv').config();
const redis = require("redis");
const { promisify } = require("util");
const _ = require('lodash');
const {REDIS_HOST, REDIS_PORT} = process.env;
const logger = require('./logger')

const client = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT
});

client.on("error", function(error) {
  console.error(error);
});


const bindList = ['get', 'set', 'del', 'flushall', 'incr', 'keys', 'sadd', 'sdiff', 'smembers', 'sismember', 'hmset', 'hgetall', 'exists', 'hget'];
const asyncClient = {};

try {
  _.map(bindList, b => {
    asyncClient[b] = promisify(client[b].bind(client));
  });
} catch (e) {
  console.log(e);
}


const __init = async () => {
  logger.info('Shopify shop init');
  await asyncClient.set('jongho-test.myshopify.com', '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');
  await asyncClient.set('ob-test-1.myshopify.com', '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');
  await asyncClient.set('dev-sellerhub.myshopify.com', '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');
  await asyncClient.set('sh-usa.myshopify.com', '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');
  await asyncClient.set('ob-test-3.myshopify.com', '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');

}

asyncClient.__init = __init;


const TokenManager = {
  get: async (shop) => {
    // todo: shop is shopify shop name form eg 'striders-ob.myshopify.com'
    const token = await asyncClient.get(shop);
    return token ? token : new Error(`not found token for ${shop}`);
  },

  set: async (shop, token) => {
    const result = await asyncClient.set(shop, token);
    return result === 'OK';
  },

  del: async (shop) => {
    await asyncClient.del(shop);
  },

  __flushall: async () => {
    await asyncClient.flushall();
  }
}

async function test() {
  console.log(await TokenManager.get('striders-ob.myshopify.com'));
  console.log(await TokenManager.set('key', 'value'));
  console.log(await TokenManager.get('key'));
  console.log(await TokenManager.del('a'));
  console.log(await TokenManager.del('key'));
  await TokenManager.__flushall();
  console.log(await TokenManager.get('key'));

}

// asyncClient.__init();

module.exports = {
  TokenManager,
  redis: asyncClient
};



