const swaggerUi = require('swagger-ui-express')
const swaggereJsdoc = require('swagger-jsdoc');
const express = require('express');


const router = express.Router();

const options = {
//swagger문서 설정
  swaggerDefinition: {
    info: {
      title: 'Test API',
      version: '1.0.0',
      description: 'Test API with express',
    },
    host: 'localhost:3000',
    basePath: '/'
  },
//swagger api가 존재하는 곳 입니다.
  apis: [require('path').join(__dirname + '../routes/index.js')]
};

const specs = swaggereJsdoc({
//swagger문서 설정
  swaggerDefinition: {
    info: {
      title: 'Test API',
      version: '1.0.0',
      description: 'Test API with express',
    },
    host: 'localhost:3000',
    basePath: '/'
  },
//swagger api가 존재하는 곳 입니다.
  apis: [require('path').join(__dirname + '/../routes/*.js'), require('path').join(__dirname + '/../spec/*.yaml')]
});

router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

module.exports = router;